<?php
use Monolog\ErrorHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SwiftMailerHandler;
use Monolog\Logger;

require __DIR__ . '/vendor/autoload.php';
include_once 'proxy.php';
// Monolog
$logger = new Logger('proxyWorker');
$logger->pushHandler(new StreamHandler(__DIR__ . '/my_app.log', Logger::DEBUG));
ErrorHandler::register($logger);

// Create the Transport
$transport = (new Swift_SmtpTransport('smtp.googlemail.com', 465, 'ssl'))->setUsername('foxer2010@gmail.com')->setPassword('pftzqatbvikwbxgv');
// Create the Mailer using your created Transport
$mailer  = new Swift_Mailer($transport);
$message = (new Swift_Message('Wonderful Subject'))
    ->setFrom(['foxer2010@gmail.com' => 'John Doe'])
    ->setTo(['foxer2010@gmail.com' => 'A name'])
    ->setBody('Here is the message itself');
$logger->pushHandler(new SwiftMailerHandler($mailer, $message, Logger::DEBUG, true));

// Proxy Class
$proxyClass = new Proxy();
//If Empty CLi
if (empty($argv['1'])) {die();}
// Read CLI Proxy And Check
$result = $proxyClass->checkProxy($argv['1']);
// Echo Result
echo implode(' - ', $result) . "\n";
// Save Result
if ($result["level"] === "0") {
    file_put_contents('Die', $result['proxy'] . PHP_EOL, FILE_APPEND);
} elseif ($result["level"] === "1") {
    file_put_contents('trans', $result['proxy'] . PHP_EOL, FILE_APPEND);
} elseif ($result["level"] === "2") {
    file_put_contents('anon', $result['proxy'] . PHP_EOL, FILE_APPEND);
} elseif ($result["level"] === "3") {
    file_put_contents('elite', $result['proxy'] . PHP_EOL, FILE_APPEND);
}
// $file       = file('proxies.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
