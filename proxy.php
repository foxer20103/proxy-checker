<?php

/**
 *
 */
use Curl\Curl;

class Proxy
{
    public $azenv;
    public $proxy;
    public $info;
    public $curl;

    public function __construct()
    {
        $this->curl  = new Curl();
        $this->azenv = [
            'http://proxydb.net/judge',
            'http://www.proxyfire.net/fastenv',
            'https://httpbin.org/get?show_env',
            'https://www.proxy-listen.de/azenv.php',
            'https://ip.haschek.at',
            'http://myexternalip.com/json',
            'http://ip.haschek.at',
            'http://azenv.net/',
            'http://proxyjudge.us/azenv.php',
            'http://httpbin.org/get?show_env',
            'smtp://smtp.gmail.com',
            'smtp://aspmx.l.google.com',
            'http://ip.spys.ru/',
            'http://www.proxy-listen.de/azenv.php',
        ];
        $this->curl->setDefaultJsonDecoder($assoc = true);
        $this->curl->setConnectTimeout(5);
        $this->curl->setTimeout(30);
    }
    public function checkProxy($proxy)
    {
        list($proxyIp, $proxyPort) = explode(':', $proxy);

        $this->curl->setProxy($proxyIp, $proxyPort);

        $this->curl->get($this->azenv['1']);
        $res = $this->curl->response;

        return [
            'proxy' => $proxy,
            'level' => $this->proxyLevel($res),
            'code'  => $this->curl->getInfo(CURLINFO_HTTP_CODE),
            'time'  => $this->curl->getInfo(CURLINFO_TOTAL_TIME),
        ];
    }
    public function proxyLevel($res)
    {
        if (!empty($res) && !is_array($res)) {
            if (
                strpos($res, 'HTTP_VIA') === false &&
                strpos($res, 'HTTP-_X_FORWARDED_FOR') === false &&
                strpos($res, 'HTTP_FORWARDED') === false &&
                strpos($res, 'HTTP_X_CLUSTER_CLIENT_IP') === false &&
                strpos($res, 'HTTP_CLIENT_IP') === false &&
                strpos($res, 'HTTP_PROXY_CONNECTION') === false &&
                strpos($res, 'HTTP_X-Real_IP') === false) {
                return '3';
            } elseif (strpos($res, 'HTTP_X_FORWARDED_FOR') === false) {
                return '2';
            } else {
                return '1';
            }
        } else {
            return '0';
        }
    }
}
